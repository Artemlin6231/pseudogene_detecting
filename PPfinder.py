import argparse
from collections import Counter
import itertools
import os
import pandas as pd
from tqdm import tqdm
import subprocess
import sys


def check_tools_installed():
    """Check if required tools are installed."""
    required_tools = ["bcftools", "bedtools"]
    for tool in required_tools:
        if subprocess.call(["which", tool], stdout=subprocess.PIPE, stderr=subprocess.PIPE) != 0:
            print(f"Error: {tool} is not installed or not in PATH.")
            sys.exit(1)

def process_files(input_dir, output_dir,annotated_introns):
    """Process all files in the input directory."""
    # Check if input directory exists
    if not os.path.isdir(input_dir):
        print(f"Error: Input directory '{input_dir}' does not exist.")
        sys.exit(1)

    # Create the output directory if it doesn't exist
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Get all files in the input directory
    files = [f for f in os.listdir(input_dir) if f.endswith(".vcf.gz")]

    if not files:
        print(f"No matching files found in the directory '{input_dir}'.")
        return

    # Process each file
    for file_name in files:
        file_path = os.path.join(input_dir, file_name)
        file_id = os.path.splitext(os.path.splitext(file_name)[0])[0]

        deletions_bed = os.path.join(output_dir, f"{file_id}.deletions.bed")
        annotated_bed = os.path.join(output_dir, f"{file_id}.annotated_deletions.bed")
        final_output = os.path.join(output_dir, f"{file_id}.deletions_annotated_and_filtered.bed")

        try:
            # Extract deletions
            subprocess.run(
                [
                    "bcftools", "query", "-f", "%CHROM\t%POS\t%END\n", "-i", "SVTYPE=\"DEL\"", file_path
                ],
                stdout=open(deletions_bed, "w"),
                check=True
            )

            # Annotate deletions
            subprocess.run(
                [
                    "bedtools", "annotate", "-i", deletions_bed, "-files", annotated_introns
                ],
                stdout=open(annotated_bed, "w"),
                check=True
            )

            # Filter annotated deletions
            subprocess.run(
                [
                    "bedtools", "intersect", "-a", annotated_bed, "-b", annotated_introns, "-wa", "-wb", "-f", "0.9", "-r"
                ],
                stdout=open(final_output, "w"),
                check=True
            )

            # Clean up intermediate files
            os.remove(deletions_bed)
            os.remove(annotated_bed)

        except subprocess.CalledProcessError as e:
            print(f"Error processing file '{file_name}': {e}")

def analyze_bed_files(dir_path,output_file):
    """Analyze BED files to identify pseudogenes."""
    bed_files = []

    for file_name in os.listdir(dir_path):
        full_file_name = os.path.join(dir_path, file_name)
        bed_files.append(full_file_name)

    pseudogenes_dataframe_myself_ann = []

    for i in tqdm(range(len(bed_files))):
        link = bed_files[i]
  
        if os.path.getsize(link) == 0:
            print(f"Skipping empty file: {link}")
            continue

        df_test_bed_tools = pd.read_csv(link, sep='\t', header=None)
        if df_test_bed_tools.empty:  
            print(f"Skipping file with no data: {link}")
            continue
            
        df_test_bed_tools = df_test_bed_tools[[0, 1, 2, 3, 7]]
        df_test_bed_tools = df_test_bed_tools[~df_test_bed_tools[7].str.contains('ENSG')].reset_index(drop=True)
        genes = df_test_bed_tools[7].unique().tolist()
        dataframe_gene_deletions = []

        # Count deletions intersecting with exons for each gene
        for gene in genes:
            df_gene = df_test_bed_tools[df_test_bed_tools[7] == gene]
            deletions = df_gene[1].unique().tolist()

            count = len(deletions)
            dataframe_gene_deletions.append([gene, deletions, count])
        dataframe_gene_deletions = pd.DataFrame(dataframe_gene_deletions, columns=['gene', 'deletions', 'quantity'])

        # Remove genes with fewer than 2 deletions
        pseudogenes = dataframe_gene_deletions['gene'].unique().tolist()
        pseudogenes_dataframe_myself_ann.append([link, pseudogenes])

    pseudogenes_dataframe_myself_ann = pd.DataFrame(pseudogenes_dataframe_myself_ann, columns=['id', 'pseudogenes'])

    def extract_sample_name(string):
        suffix_length = len('.deletions.annotate.bed')
        return os.path.basename(string)[:-suffix_length]

    # Add a column with sample names
    pseudogenes_dataframe_myself_ann['names'] = pseudogenes_dataframe_myself_ann['id'].apply(extract_sample_name)

    # Flatten pseudogene list and calculate frequencies
    nested_list = pseudogenes_dataframe_myself_ann.pseudogenes.tolist()
    flat_list = list(itertools.chain.from_iterable(nested_list))
    frequency = Counter(flat_list)

    frequency_list = []
    for pseudogene, freq in frequency.items():
        frequency_list.append([pseudogene, freq])

    frequency_list = pd.DataFrame(frequency_list, columns=['pseudogenes', 'frequency'])
    frequency_list = frequency_list[~frequency_list['pseudogenes'].str.contains('RNA', case=False)]
    frequency_list['proportion'] = frequency_list['frequency'] / len(pseudogenes_dataframe_myself_ann)
    frequency_list = frequency_list.sort_values(by='frequency', ascending=False).reset_index(drop=True)

    # Add sample list for each pseudogene
    pseudogene_samples = {
        pseudogene: pseudogenes_dataframe_myself_ann[pseudogenes_dataframe_myself_ann['pseudogenes'].apply(lambda x: pseudogene in x)]['names'].tolist()
        for pseudogene in frequency_list['pseudogenes']
    }

    frequency_list['samples'] = frequency_list['pseudogenes'].map(pseudogene_samples)
    frequency_list.to_csv(output_file,sep='\t')

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i", "--input_directory",
        required=True,
        type=str,
        help="Path to the input directory containing the files to process."
    )
    parser.add_argument(
        "-o", "--output_table",
        required=True,
        type=str,
        help="Path to the output table with detecting PPs in samples."
    )
    parser.add_argument(
        "-bo", "--bed_output_directory",
        required=True,
        type=str,
        help="Path to the BED directory for additional processing."
    )
    parser.add_argument(
        "-anno", "--anno_introns",
        required=True,
        type=str,
        help="Path to the BED file with annotated introns."
    )

    # Parse arguments
    args = parser.parse_args()

    # Access arguments using args.<argument_name>
    input_dir = args.input_directory
    output_file = args.output_table
    bed_dir = args.bed_output_directory
    anno_introns = args.anno_introns

    check_tools_installed()
    process_files(input_dir, bed_dir,anno_introns)

    # Analyze BED files for pseudogenes
    pseudogene_data = analyze_bed_files(bed_dir,output_file)
    print("Pseudogene analysis completed.")
    